<?php
	include('session.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Daftar Orang-MyCRM</title>
		<link rel="stylesheet" href="../css/bootstrap.min.css">
    	<script src="../js/jquery-3.3.1.slim.min.js"></script>
    	<script src="../js/popper.min.js"></script>
    	<script src="../js/bootstrap.min.js"></script>
    	<script src="../jquery/jquery-3.3.1.min.js"></script>
    	<script>
    		function getOrang(){
    			var xmlhttp = new XMLHttpRequest();
    			xmlhttp.onreadystatechange = function(){
    				if(this.readyState == 4 && this.status == 200){
    					document.getElementById("tabelOrang").innerHTML=this.responseText;
    				}
    			};
    			xmlhttp.open("GET","../database/querymanager.php?listOrang="+1,true);
    			xmlhttp.send();
    		}
    		$(document).ready(function(){
  				getOrang();
			});
    	</script>
	</head>
	<body>
		<div class="container" style="height:100vh;padding:1px;margin-right: 0;margin-left: 0">
			<div class="row align-items-center justify-content-center" style="height:20vh;margin:0">
				<div class="col">
					<h2>Daftar Orang</h2>
				</div>
				<div class="col">
					<a class="btn btn-primary text-light" href="addorang.php">Tambahkan Orang Baru</a>
					<a class="btn btn-primary text-light" href="dashboard.php">Back</a>
				</div>
			</div>
			<div class="row align-items-center justify-content-center" style="height:80vh;margin:0">
				<div class="col">
					<?php
						if(isset($_SESSION['update_success'])){
							echo "<p class='alert alert-success'>".$_SESSION['update_success']."</p>";
							unset($_SESSION['update_success']);
						}
					?>
					<table class="table table-hover">
						<thead>
							<tr>
								<th scope="col">Id Orang</th>
								<th scope="col">Nama</th>
								<th scope="col">Jenis Kelamin</th>
								<th scope="col">TanggalLahir</th>
								<th scope="col">Umur</th>
								<th scope="col">Alamat</th>
								<th scope="col">Lokasi</th>
								<th scope="col">Status Nikah</th>
								<th scope="col">Penghasilan</th>
								<th scope="col">Pekerjaan</th>
								<th scope="col">Alamat Email</th>
								<th scope="col">Opsi</th>
							</tr>
						</thead>
						<tbody id="tabelOrang">
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>
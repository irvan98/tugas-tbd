<?php
	include('session.php');

	if(!isset($_SESSION['login_user']))
	{
		header("location: login.php");
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Dashboard Admin - MyCRM</title>
	<link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../js/jquery-3.3.1.slim.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../jquery/jquery-3.3.1.min.js"></script>
    <script>
    	//menggunakan XMLHTTPRequest agar tidak harus refresh page
    	function getKlienByPenghasilan(){
    		var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function(){
    			if(this.readyState == 4 && this.status == 200){
    				document.getElementById("tabelKlien").innerHTML=this.responseText;
    			}
    		};

    		var lower_penghasilan = $("#lower_penghasilan_klien").val();
    		var upper_penghasilan = $("#upper_penghasilan_klien").val();
    		var query_param = [];
    		query_param.push(lower_penghasilan);
    		query_param.push(upper_penghasilan);

    		xmlhttp.open("GET","../database/querymanager.php?listKlienRentangPenghasilan="+query_param,true);
    		xmlhttp.send();
    	}
    	function getKlienByInvestasi(){
    		var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function(){
    			if(this.readyState == 4 && this.status == 200){
    				document.getElementById("tabelKlien").innerHTML=this.responseText;
    			}
    		};
    		var lower_investasi = $("#lower_investasi").val();
    		var upper_investasi = $("#upper_investasi").val();
    		var query_param = [];
    		query_param.push(lower_investasi);
    		query_param.push(upper_investasi);

    		xmlhttp.open("GET","../database/querymanager.php?listKlienRentangInvestasi="+query_param,true);
    		xmlhttp.send();
    	}
    	function getOrangByPenghasilan(){
    		var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function(){
    			if(this.readyState == 4 && this.status == 200){
    				document.getElementById("tabelOrang").innerHTML=this.responseText;
    			}
    		};
    		var lower_penghasilan= $("#lower_penghasilan").val();
    		var upper_penghasilan= $("#upper_penghasilan").val();

    		var query_param = [];
    		query_param.push(lower_penghasilan);
    		query_param.push(upper_penghasilan);

    		xmlhttp.open("GET","../database/querymanager.php?listOrangRentangPenghasilan="+query_param,true);
    		xmlhttp.send();
		}
		function getEvent(){
    		var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function(){
    			if(this.readyState == 4 && this.status == 200){
    				document.getElementById("tabelEvent").innerHTML=this.responseText;
    			}
			};

			var jumlah_hari= $("#jumlah_hari").val();

			var query_param = [];
			query_param.push(jumlah_hari);

			xmlhttp.open("GET","../database/querymanager.php?listEvent="+query_param,true);
			xmlhttp.send();
		}
		function getRelasi(){
			var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function(){
    			if(this.readyState == 4 && this.status == 200){
    				document.getElementById("tabelRelasi").innerHTML=this.responseText;
    			}
			};

			var id_klien= $("#id_klien").val();

			var query_param = [];
			query_param.push(id_klien);

			xmlhttp.open("GET","../database/querymanager.php?listRelasi="+query_param,true);
			xmlhttp.send();
		}
		function getRata2Inv(){
			var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function(){
    			if(this.readyState == 4 && this.status == 200){
    				document.getElementById("tabelRata2Inv").innerHTML=this.responseText;
    			}
			};

			var nama_lokasi= $("#nama_lokasi").val();

			var query_param = [];
			query_param.push(nama_lokasi);

			xmlhttp.open("GET","../database/querymanager.php?rata2Inv="+query_param,true);
			xmlhttp.send();
		}
		function getAllRata2Inv(){
			var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function(){
    			if(this.readyState == 4 && this.status == 200){
    				document.getElementById("tabelRata2Inv").innerHTML=this.responseText;
    			}
			};
			xmlhttp.open("GET","../database/querymanager.php?allRata2Inv="+query_param,true);
			xmlhttp.send();
		}
		function getRata2Umur(){
			var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function(){
    			if(this.readyState == 4 && this.status == 200){
    				document.getElementById("tabelRata2Umur").innerHTML=this.responseText;
    			}
			};

			var nama_lokasi= $("#nama_lokasi").val();

			var query_param = [];
			query_param.push(nama_lokasi);

			xmlhttp.open("GET","../database/querymanager.php?rata2Umur="+query_param,true);
			xmlhttp.send();
		}
		function getAllRata2Umur(){
			var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function(){
    			if(this.readyState == 4 && this.status == 200){
    				document.getElementById("tabelRata2Umur").innerHTML=this.responseText;
    			}
			};
			xmlhttp.open("GET","../database/querymanager.php?allRata2Umur="+query_param,true);
			xmlhttp.send();
		}
		function getPersenNikah(){
			var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function(){
    			if(this.readyState == 4 && this.status == 200){
    				document.getElementById("tabelPersenNikah").innerHTML=this.responseText;
    			}
			};

			var nama_lokasi= $("#nama_lokasi").val();

			var query_param = [];
			query_param.push(nama_lokasi);

			xmlhttp.open("GET","../database/querymanager.php?persenNikah="+query_param,true);
			xmlhttp.send();
		}
		function getAllPersenNikah(){
			var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function(){
    			if(this.readyState == 4 && this.status == 200){
    				document.getElementById("tabelPersenNikah").innerHTML=this.responseText;
    			}
			};
			xmlhttp.open("GET","../database/querymanager.php?allPersenNikah="+query_param,true);
			xmlhttp.send();
		}
		function getPerbandingan(){
			var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function(){
    			if(this.readyState == 4 && this.status == 200){
    				document.getElementById("tabelKarakteristik1").innerHTML=this.responseText;
    			}
			};

			var nama_lokasi= $("#nama_lokasi1").val();

			var query_param = [];
			query_param.push(nama_lokasi);

			xmlhttp.open("GET","../database/querymanager.php?karakteristik1="+query_param,true);
			xmlhttp.send();

			var xmlhttp2 = new XMLHttpRequest();
    		xmlhttp2.onreadystatechange = function(){
    			if(this.readyState == 4 && this.status == 200){
    				document.getElementById("tabelKarakteristik2").innerHTML=this.responseText;
    			}
			};

			var nama_lokasi2= $("#nama_lokasi2").val();

			var query_param2 = [];
			query_param2.push(nama_lokasi2);

			xmlhttp2.open("GET","../database/querymanager.php?karakteristik2="+query_param2,true);
			xmlhttp2.send();
		}
    </script>
</head>
<body>
	<div class="container" style="height:100vh">
		<div class="row">
			<div class="col p-3">
				<h2>Dashboard</h2>
			</div>
			<div class="col p-3">
				<h4>Welcome, <?php echo $namaCS; ?></h4> 
				<a href="logout.php" class="btn btn-primary text-light">Logout</a>
			</div>
		</div>
		<div class="row">
			<div class="col p-3">
				<a class="btn btn-primary text-light" href="listmember.php">List Klien</a>
				<a class="btn btn-primary text-light" href="listorang.php">List Orang</a>
				<a class="btn btn-primary text-light" href="laporan.php">Laporan</a>
				<a class="btn btn-primary text-light" href="listhistori.php">Histori Perubahan</a>
				<br>
				<br>
				<div id="accordion">
					<div class="card">
						<div class="card-header" id="heading_one">
							<h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapse_one" aria-expanded="false" aria-controls="collapse_one">
									Cari Klien dengan rentang nilai investasi atau penghasilan tertentu
								</button>
							</h5>
						</div>
						<div id="collapse_one" class="collapse" data-parent="#accordion">
							<div class="card-body">
								<div class="row">
									<div class="col-5">
									<input type="number" class="form-control" id="lower_investasi" name="batas_bawah" placeholder="batas Bawah Nilai Investasi">
									<br>
									<input type="number" class="form-control" id="upper_investasi" name="batas_atas" placeholder="Batas Atas Nilai Investasi">
									<br>
									<button class="btn btn-primary text-light" id="btn1" name="rentang" onclick="getKlienByInvestasi()">Cari Klien Berdasarkan Nilai Investasi</button>
								</div>
								<div class="col-2" style="align-content:center;">
									<h1>Atau</h1>
								</div>
								<div class="col-5">
									<input type="number" class="form-control" id="lower_penghasilan_klien" name="batas_bawah" placeholder="batas Bawah Penghasilan">
									<br>
									<input type="number" class="form-control" id="upper_penghasilan_klien" name="batas_atas" placeholder="Batas Atas Penghasilan">
									<br>
									<button class="btn btn-primary text-light" id="btn2" name="rentang" onclick="getKlienByPenghasilan()">Cari Klien Berdasarkan Penghasilan</button>
								</div>
								</div>
								<br>
								<div>
									<table class="table">
										<thead>
											<tr>
												<th scope="col">Id Klien</th>
												<th scope="col">Nama Klien</th>
												<th scope="col">Nilai Investasi</th>
												<th scope="col">Penghasilan</th>
												<th scope="col">Status</th>
											</tr>
										</thead>
										<tbody id="tabelKlien">
							
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header" id="heading_two">
							<h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapse_two" aria-expanded="false" aria-controls="collapse_two">
								Cari Orang dengan rentang penghasilan tertentu
								</button>
							</h5>
						</div>
						<div id="collapse_two" class="collapse" data-parent="#accordion">
							<div class="card-body">
								<div>
									<input type="number" class="form-control" id="lower_penghasilan" name="batas_bawah" placeholder="batas Bawah Penghasilan">
									<br>
									<input type="number" class="form-control" id="upper_penghasilan" name="batas_atas" placeholder="Batas Atas Penghasilan">
									<br>
									<button class="btn btn-primary text-light" id="btn3" name="rentang" onclick="getOrangByPenghasilan()">Cari Orang</button>
								</div>
								<br>
								<div>
									<table class="table">
										<thead>
											<tr>
												<th scope="col">Id Orang</th>
												<th scope="col">Nama Orang</th>
												<th scope="col">Penghasilan</th>
											</tr>
										</thead>
										<tbody id="tabelOrang">
							
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header" id="heading_three">
							<h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapse_three" aria-expanded="false" aria-controls="collapse_three">
								Lihat Event Penting dalam beberapa hari kedepan
								</button>
							</h5>
						</div>
						<div id="collapse_three" class="collapse" data-parent="#accordion">
							<div class="card-body">
								<div>
									<input type="number" class="form-control" id="jumlah_hari" name="hari" placeholder="jumlah hari kedepan">
									<br>
									<button class="btn btn-primary text-light" id="btn4" name="event" onclick="getEvent()">Cari Event</button>
								</div>
								<br>
								<div>
									<table class="table">
										<thead>
											<tr>
												<th scope="col">Id Orang</th>
												<th scope="col">Tanggal</th>
												<th scope="col">Event</th>
											</tr>
										</thead>
										<tbody id="tabelEvent">
							
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header" id="heading_four">
							<h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapse_four" aria-expanded="false" aria-controls="collapse_four">
								Cari Relasi dari Seorang Klien
								</button>
							</h5>
						</div>
						<div id="collapse_four" class="collapse" data-parent="#accordion">
							<div class="card-body">
								<div>
									<input type="number" class="form-control" id="id_klien" name="idKlien" placeholder="id klien">
									<br>
									<button class="btn btn-primary text-light" id="btn5" name="relasi" onclick="getRelasi()">Cari Relasi</button>
								</div>
								<br>
								<div>
									<table class="table">
										<thead>
											<tr>
												<th scope="col">Id Orang</th>
												<th scope="col">Nama</th>
												<th scope="col">Hubungan</th>
											</tr>
										</thead>
										<tbody id="tabelRelasi">
							
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="card">
						<div class="card-header" id="heading_five">
							<h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapse_five" aria-expanded="false" aria-controls="collapse_five">
								Cari rata-rata nilai investasi klien di suatu daerah atau seluruh daerah
								</button>
							</h5>
						</div>
						<div id="collapse_five" class="collapse" data-parent="#accordion">
							<div class="card-body">
								<div>
									<input type="text" class="form-control" id="nama_lokasi" name="lokasi" placeholder="Input nama lokasi">
									<br>
									<button class="btn btn-primary text-light" id="btn6" name="rata2Inv" onclick="getRata2Inv()">Cari rata-rata nilai investasi klien di suatu daerah</button>
									<button class="btn btn-primary text-light" id="btn7" name="allRata2Inv" onclick="getAllRata2Inv()">Cari seluruh rata-rata nilai investasi klien</button>
								</div>
								<br>
								<div>
									<table class="table">
										<thead>
											<tr>
												<th scope="col">Rata-rata Nilai Investasi</th>
											</tr>
										</thead>
										<tbody id="tabelRata2Inv">
							
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="card">
						<div class="card-header" id="heading_six">
							<h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapse_six" aria-expanded="false" aria-controls="collapse_six">
								Cari rata-rata umur klien di suatu daerah atau seluruh daerah
								</button>
							</h5>
						</div>
						<div id="collapse_six" class="collapse" data-parent="#accordion">
							<div class="card-body">
								<div>
									<input type="text" class="form-control" id="nama_lokasi" name="lokasi" placeholder="Input nama lokasi">
									<br>
									<button class="btn btn-primary text-light" id="btn8" name="rata2Umur" onclick="getRata2Umur()">Cari rata-rata umur klien di suatu daerah</button>
									<button class="btn btn-primary text-light" id="btn9" name="allRata2Umur" onclick="getAllRata2Umur()">Cari seluruh rata-rata umur klien</button>
								</div>
								<br>
								<div>
									<table class="table">
										<thead>
											<tr>
												<th scope="col">Rata-rata Umur</th>
											</tr>
										</thead>
										<tbody id="tabelRata2Umur">
							
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="card">
						<div class="card-header" id="heading_seven">
							<h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapse_seven" aria-expanded="false" aria-controls="collapse_seven">
								Cari persentase klien yang menikah di suatu daerah atau seluruh daerah
								</button>
							</h5>
						</div>
						<div id="collapse_seven" class="collapse" data-parent="#accordion">
							<div class="card-body">
								<div>
									<input type="text" class="form-control" id="nama_lokasi" name="lokasi" placeholder="Input nama lokasi">
									<br>
									<button class="btn btn-primary text-light" id="btn8" name="persenNikah" onclick="getPersenNikah()">Cari persentase klien yang menikah di suatu daerah</button>
									<button class="btn btn-primary text-light" id="btn9" name="allPersenNikah" onclick="getAllPersenNikah()">Cari persentase klien menikah di seluruh daerah</button>
								</div>
								<br>
								<div>
									<table class="table">
										<thead>
											<tr>
												<th scope="col">Persentase menikah</th>
											</tr>
										</thead>
										<tbody id="tabelPersenNikah">
							
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="card">
						<div class="card-header" id="heading_eight">
							<h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapse_eight" aria-expanded="false" aria-controls="collapse_eight">
								Perbandingan karakteristik antar daerah
								</button>
							</h5>
						</div>
						<div id="collapse_eight" class="collapse" data-parent="#accordion">
							<div class="card-body">
								<div>
									<input type="text" class="form-control" id="nama_lokasi1" name="lokasi1" placeholder="Input nama lokasi pertama">
									<input type="text" class="form-control" id="nama_lokasi2" name="lokasi2" placeholder="Input nama lokasi kedua">
									<br>
									<button class="btn btn-primary text-light" id="btn8" name="persenNikah" onclick="getPerbandingan()">Bandingkan</button>
								</div>
								<br>
								<div>
									<table class="table">
										<thead>
											<tr>
												<th scope="col">Rata-rata nilai investasi daerah pertama</th>
												<th scope="col">Rata-rata umur daerah pertama</th>
												<th scope="col">persentase menikah daerah pertama</th>
											</tr>
										</thead>
										<tbody id="tabelKarakteristik1">
							
										</tbody>
									</table>
									<table class="table">
										<thead>
											<tr>
												<th scope="col">Rata-rata nilai investasi daerah kedua</th>
												<th scope="col">Rata-rata umur daerah kedua</th>
												<th scope="col">persentase menikah daerah kedua</th>
											</tr>
										</thead>
										<tbody id="tabelKarakteristik2">
							
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="../css/bootstrap.min.css">
    	<script src="../js/jquery-3.3.1.slim.min.js"></script>
    	<script src="../js/popper.min.js"></script>
    	<script src="../js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container" style="height: 100vh">
			<div class="row align-items-center justify-content-center" style="height: 100vh">
				<div class="col">
					<h1>Initialization Page</h1>
					<p>Untuk membuat tabel-tabel dan data yang dibutuhkan</p>
					<p>NOTE! pastikan terdapat database bernama <strong>tugasuastbd</strong> di phpmyadmin anda</p>
					<form method="POST" action="../database/seeder.php">
						<input class="btn btn-primary" type="submit" name="btnTabel" value="Buat Tabel"/>
					</form>
					<br>
					<form method="POST" action="../database/seeder.php">
						<input class="btn btn-primary" type="submit" name="btnInsertData" value="Generate Dummy Data"/>
					</form>
					<br>
					<a class="btn btn-primary text-light" href="dashboard.php">To Dashboard</a>
				</div>
			</div>
		</div>		
	</body>
</html>
<!DOCTYPE html>
<html>
	<head>
		<title>Tambah Member-MyCRM</title>
		<link rel="stylesheet" href="../css/bootstrap.min.css">
    	<script src="../js/jquery-3.3.1.slim.min.js"></script>
    	<script src="../js/popper.min.js"></script>
    	<script src="../js/bootstrap.min.js"></script>
    	<script src="../jquery/jquery-3.3.1.min.js"></script>
    	<script>
    		function getNamaOrang(idOrang){
    			var xmlhttp = new XMLHttpRequest();
    			xmlhttp.onreadystatechange = function(){
    				if(this.readyState == 4 && this.status == 200){
    					document.getElementById("nama").value=this.responseText;
    				}
    			};
    			xmlhttp.open("GET","../database/querymanager.php?idOrang="+idOrang,true);
    			xmlhttp.send();
    		}

    		function getIdOrang(){
    			var xmlhttp = new XMLHttpRequest();
    			xmlhttp.onreadystatechange = function(){
    				if(this.readyState == 4 && this.status == 200){
    					document.getElementById("listId").innerHTML = this.responseText;
    				}
    			};
    			xmlhttp.open("GET","../database/querymanager.php?listId="+1,true);
    			xmlhttp.send();
    		}

    		function getIdCS(){
    			var xmlhttp = new XMLHttpRequest();
    			xmlhttp.onreadystatechange = function(){
    				if(this.readyState == 4 && this.status == 200){
    					document.getElementById("listCS").innerHTML = this.responseText;
    				}
    			};
    			xmlhttp.open("GET","../database/querymanager.php?listCS="+1,true);
    			xmlhttp.send();
    		}
    		$(document).ready(function(){
  				getIdOrang();
  				getIdCS();
			});
    	</script>
	</head>
	<body>
		<div class="container" style="height:100vh">
			<div class="row align-items-center justify-content-center" style="height:100vh">
				<div class="col-5">
					<h2>Tambahkan Klien Baru</h2>
					<form method="POST" action="../database/querymanager.php">
						<div class="form-group">
						<label>Id Orang</label>
						<select name="attributKlien[idOrang]" class="form-control" onchange="getNamaOrang(this.value)" id="listId">
							
						</select>
						</div>
						<div class="form-group">
						<label>Nama Orang</label>
						<input type="text" class="form-control" id="nama" placeholder="Nama Orang" readonly>
						</div>
						<div class="form-group">
							<label>Nilai Investasi</label>
							<input type="number" class="form-control" name="attributKlien[nilaiInvestasi]" placeholder="Nilai Investasi">
						</div>
						<div class="form-group">
							<label>Id Customer Service</label>
							<select name="attributKlien[idCS]" class="form-control" id="listCS"></select>
							<input type="hidden" name="attributKlien[deleted]" value=0>
						</div>
						<input type="submit" class="btn btn-primary" name="btnAddKlien" value="Tambahkan Klien baru" />
					</form>
				</div>
			</div>
		</div>
	</body>
</html>
<?php
	include('../database/MySQLDB.php');
	include('session.php');
	if(isset($_GET['idOrang'])){
		$idOrang = $_GET['idOrang'];
		$query = "SELECT nama,jenisKelamin,tanggalLahir,umur,alamat,lokasi,statusNikah,penghasilan,pekerjaan,alamatEmail FROM Orang WHERE idOrang='$idOrang' ";
		$data = $database->executeQuery($query);
		$oldvalue=$data[0];
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Ubah Data Orang</title>
	<link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../js/jquery-3.3.1.slim.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../jquery/jquery-3.3.1.min.js"></script>
    <script>
    	function getAge(){
			var userbdate=document.getElementById('bdate').value;
			var userage=document.getElementById('umur');

			var today = new Date();
			var objDate = new Date(userbdate);

			var diff = today.getTime()-objDate.getTime();
			var objDiff = new Date(diff);
			var age= objDiff.getFullYear()-1970;

			if(age >= 0){
				userage.value=age;
			}
    	}
    	function getRegion(){
    		var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function(){
    			if(this.readyState == 4 && this.status == 200){
    				document.getElementById("lokasi").innerHTML=this.responseText;
    			}
    		};
    		xmlhttp.open("GET","../database/querymanager.php?listRegion="+1,false);
    		xmlhttp.send();
    	}

    	function showCurrentPersonData(){
    		$("#idorang").val("<?php echo $_GET['idOrang']?>");
    		$("#fname").val("<?php echo $oldvalue['nama']?>");

    		var gender = "<?php echo $oldvalue['jenisKelamin']?>";
  			if(gender == 0){
  				$("#genderW").attr('checked',true);
  			}else{
  				$("#genderP").attr('checked',true);
  			}

  			$("#bdate").val("<?php echo $oldvalue['tanggalLahir']?>");
  			$("#umur").val("<?php echo $oldvalue['umur']?>");
  			$("#alamat").val("<?php echo $oldvalue['alamat']?>");
  			
  			//select lokasi
  			var val = <?php echo $oldvalue['lokasi']?>;

  			$("#lokasi option[value="+val+"]").attr("selected", "selected");
 

  			var statusNikah = "<?php echo $oldvalue['statusNikah']?>";
  			if(statusNikah == 0){
  				$("#statusNikah2").attr('checked',true);
  			}else{
  				$("#statusNikah1").attr('checked',true);
  			}

  			$("#penghasilan").val("<?php echo $oldvalue['penghasilan']?>");
  			$("#pekerjaan").val("<?php echo $oldvalue['pekerjaan']?>");
  			$("#email").val("<?php echo $oldvalue['alamatEmail']?>");
    	}

    	//function untuk menyimpan nilai-nilai awal
    	function keepOldData(){
    		$("#old_fname").val("<?php echo $oldvalue['nama']?>");
    		$("#old_gender").val("<?php echo $oldvalue['jenisKelamin']?>");
    		$("#old_bdate").val("<?php echo $oldvalue['tanggalLahir']?>");
    		$("#old_umur").val("<?php echo $oldvalue['umur']?>");
    		$("#old_alamat").val("<?php echo $oldvalue['alamat']?>");
    		$("#old_lokasi").val("<?php echo $oldvalue['lokasi']?>");
    		$("#old_mstatus").val("<?php echo $oldvalue['statusNikah']?>");
    		$("#old_penghasilan").val("<?php echo $oldvalue['penghasilan']?>");
  			$("#old_pekerjaan").val("<?php echo $oldvalue['pekerjaan']?>");
  			$("#old_email").val("<?php echo $oldvalue['alamatEmail']?>");
  			$("#idCS").val("<?php echo $idCS ?>");
    	}
    	
    	$(document).ready(function(){
  			getRegion();
  			showCurrentPersonData();
  			keepOldData();
		});
  		
    </script>
</head>
<body>
	<div class="container" style="height:100vh">
		<div class="row align-items-center justify-content-center" style="height:100vh">
			<div class="col-5">
				<h2>Ubah Data Orang</h2>
				<form method="POST" action="../database/editorang.php">
					<input type="hidden" name="idOrang" id="idorang">
					<input type="hidden" name="idCS" id="idCS">
					<input type="hidden" name="oldvalue[nama]"  id="old_fname">
					<input type="hidden" name="oldvalue[jenisKelamin]" id="old_gender">
					<input type="hidden" name="oldvalue[tanggalLahir]" id="old_bdate">
					<input type="hidden" name="oldvalue[umur]" id="old_umur">
					<input type="hidden" name="oldvalue[alamat]" id="old_alamat">
					<input type="hidden" name="oldvalue[lokasi]" id="old_lokasi">
					<input type="hidden" name="oldvalue[statusNikah]" id="old_mstatus">
					<input type="hidden" name="oldvalue[penghasilan]" id="old_penghasilan">
					<input type="hidden" name="oldvalue[pekerjaan]" id="old_pekerjaan">
					<input type="hidden" name="oldvalue[alamatEmail]" id="old_email">

					<div class="form-group">
					<label for="fname">Nama Lengkap</label>
					<input type="text" name="newvalue[nama]" class="form-control" id="fname" placeholder="Nama Lengkap" required>
					</div>
					<div class="form-group">
					<label for="gender">Jenis Kelamin</label>
						<div>
							<label> <input type="radio" name="newvalue[jenisKelamin]" value="1" id="genderP">Pria<br></label>
							&nbsp;
							<label> <input type="radio" name="newvalue[jenisKelamin]" value="0" id="genderW">Wanita<br></label>
						</div>
					</div> 
					<div class="form-group">
					<label for="bdate">Tanggal Lahir</label>
					<input type="date" name="newvalue[tanggalLahir]" class="form-control" id="bdate" required onchange="getAge()">
					</div>
					<div class="form-group">
					<label for="umur">Umur</label>
					<input type="number" name="newvalue[umur]" value="" class="form-control" id="umur" placeholder="Umur" readonly>
					</div>
					<div class="form-group">
					<label for="alamat">Alamat</label>
					<input type="text" name="newvalue[alamat]" class="form-control" id="alamat" placeholder="Alamat" required>
					</div>
					<div class="form-group">
					<label for="lokasi">Lokasi</label>
							<select name="newvalue[lokasi]" class="form-control" id="lokasi" required>
								
							</select>
					</div>
					<div class="form-group">
					<label for="statusNikah">Status Nikah</label>
						<div>
							<label> <input type="radio" name="newvalue[statusNikah]" value="1" id="statusNikah1">Sudah Nikah<br></label>
							&nbsp;
							<label> <input type="radio" name="newvalue[statusNikah]" value="0" id="statusNikah2">Belum Nikah<br></label>
						</div>
					</div>
					<div class="form-group">
					<label for="penghasilan">Penghasilan</label>
					<input type="number" name="newvalue[penghasilan]" class="form-control" id="penghasilan" placeholder="Pengasilan" required>
					</div>
					<div class="form-group">
					<label for="pekerjaan">Pekerjaan</label>
					<input type="text" name="newvalue[pekerjaan]" class="form-control" id="pekerjaan" placeholder="Pekerjaan" required>
					</div> 
					<div class="form-group">
					<label for="email">Alamat Email</label>
					<input type="email" name="newvalue[alamatEmail]" class="form-control" id="email" placeholder="Alamat Email" required>
					</div>
					<input type="submit" class="btn btn-primary" name="btnEditOrang" value="Simpan perubahan"/>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
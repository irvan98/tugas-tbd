<?php
	include('session.php');

	if(!isset($_SESSION['login_user']))
	{
		header("location: login.php");
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Laporan-MyCRM</title>
	<link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../js/jquery-3.3.1.slim.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../jquery/jquery-3.3.1.min.js"></script>
    <script>
    	function getAvgInvestasiByProvinsi(){
    		var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function(){
    			if(this.readyState == 4 && this.status == 200){
    				document.getElementById("tbl_avg_investasi_region").innerHTML=this.responseText;
    			}
    		};
    		xmlhttp.open("GET","../database/laporanmanager.php?list_avg_investasi_prov="+1,true);
    		xmlhttp.send();
    	}

    	$(document).ready(function(){
    		getAvgInvestasiByProvinsi();
		});
    </script>
</head>
<body>
	<div class="container" style="height: 100vh">
		<div class="row">
			<div class="col p-3">
				<h2>Modul Laporan</h2>
			</div>
			<div class="col p-3">
				<h4>Welcome, <?php echo $namaCS; ?></h4> 
				<a href="logout.php" class="btn btn-primary text-light">Logout</a>
				<a class="btn btn-primary text-light" href="dashboard.php">Back</a>
			</div>
		</div>
		<div class="row">
			<div class="col p-3">
				<div id="accordion">
					<div class="card">
						<div class="card-header" id="heading_one">
							<h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapse_one" aria-expanded="false" aria-controls="collapse_one">
									Cari Rata-rata nilai Investasi Klien di setiap daerah
								</button>
							</h5>
						</div>
						<div id="collapse_one" class="collapse" data-parent="#accordion">
							<div class="card-body">
								<div>
									<table class="table">
										<thead>
											<tr>
												<th scope="col">Nama Daerah</th>
												<th scope="col">Rata-rata Nilai Investasi</th>
											</tr>
										</thead>
										<tbody id="tbl_avg_investasi_region">
											
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="card">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
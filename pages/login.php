<?php
	include('connection.php');

	if(isset($_SESSION['login_user']))
	{
		header("location: dashboard.php");
	}
?>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../js/jquery-3.3.1.slim.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container" style="height:100vh">
		<div class="row align-items-center justify-content-center" style="height:100vh">
			<div class="col-5">
				<h1>myCRM</h1>
				<form method="POST">
					<div class="form-group">
						<label for="usernameInput">Username</label>
						<input type="text" name="username" class="form-control" id="usernameInput" placeholder="Username">
					</div>
					<div class="form-group">
						<label for="passwordInput">Password</label>
						<input type="password" name="password" class="form-control" id="passwordInput" placeholder="Password">
					</div>
					<button type="submit" name="submit" class="btn btn-primary">Submit</button>
				</form>
				<?php if(isSet($_POST['submit'])){echo $error;}?>
			</div>
		</div>
	</div>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../js/jquery-3.3.1.slim.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript">
    	function getAge(){
			var userbdate=document.getElementById('bdate').value;
			var userage=document.getElementById('umur');

			var today = new Date();
			var objDate = new Date(userbdate);

			var diff = today.getTime()-objDate.getTime();
			var objDiff = new Date(diff);
			var age= objDiff.getFullYear()-1970;

			if(age >= 0){
				userage.value=age;
			}
    	}

    	function getRegion(){
    		var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function(){
    			if(this.readyState == 4 && this.status == 200){
    				document.getElementById("lokasi").innerHTML=this.responseText;
    			}
    		};
    		xmlhttp.open("GET","../database/querymanager.php?listRegion="+1,true);
    		xmlhttp.send();
    	}

    	$(document).ready(function(){	
  			getRegion();
		});
    </script>
</head>
<body>
	<div class="container" style="height:100vh">
		<div class="row align-items-center justify-content-center" style="height:100vh">
			<div class="col-5">
				<h2>Tambahkan Orang Baru</h2>
				<form method="POST" action="../database/querymanager.php">
					<div id="form_add_orang">
						<div class="form-group">
					<label for="fname">Nama Lengkap</label>
					<input type="text" name="attributOrang[namaLengkap]" class="form-control" onchange="test(this.value)" id="fname" placeholder="Nama Lengkap" required>
					</div>
					<div class="form-group">
					<label for="gender">Jenis Kelamin</label>
						<div>
							<label> <input type="radio" name="attributOrang[jenisKelamin]" value="1" id="genderP">Pria<br></label>
							&nbsp;
							<label> <input type="radio" name="attributOrang[jenisKelamin]" value="0" id="genderW">Wanita<br></label>
						</div>
					</div> 
					<div class="form-group">
					<label for="bdate">Tanggal Lahir</label>
					<input type="date" name="attributOrang[tanggalLahir]" class="form-control" id="bdate" required onchange="getAge()">
					</div>
					<div class="form-group">
					<label for="umur">Umur</label>
					<input type="number" name="attributOrang[umur]" value="" class="form-control" id="umur" placeholder="Umur" readonly>
					</div>
					<div class="form-group">
					<label for="alamat">Alamat</label>
					<input type="text" name="attributOrang[alamat]" class="form-control" id="alamat" placeholder="Alamat" required>
					</div>
					<div class="form-group">
					<label for="lokasi">Lokasi</label>
							<select name="attributOrang[lokasi]" class="form-control" id="lokasi" required>
								
							</select>
					</div>
					<div class="form-group">
					<label for="statusNikah">Status Nikah</label>
						<div>
							<label> <input type="radio" name="attributOrang[statusNikah]" value="1" id="statusNikah1">Sudah Nikah<br></label>
							&nbsp;
							<label> <input type="radio" name="attributOrang[statusNikah]" value="0" id="statusNikah2">Belum Nikah<br></label>
						</div>
					</div>
					<div class="form-group">
					<label for="penghasilan">Penghasilan</label>
					<input type="number" name="attributOrang[penghasilan]" class="form-control" id="penghasilan" placeholder="Pengasilan" required>
					</div>
					<div class="form-group">
					<label for="pekerjaan">Pekerjaan</label>
					<input type="text" name="attributOrang[pekerjaan]" class="form-control" id="pekerjaan" placeholder="Pekerjaan" required>
					</div> 
					<div class="form-group">
					<label for="email">Alamat Email</label>
					<input type="email" name="attributOrang[alamatEmail]" class="form-control" id="email" placeholder="Alamat Email" required>
					</div>
					</div>
					<input type="submit" class="btn btn-primary" name="btnAddOrang" value="Tambahkan Orang baru" />
				</form>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	
</script>
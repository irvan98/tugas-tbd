<?php
include('session.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Histori Perubahan-MyCRM</title>
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<script src="../js/jquery-3.3.1.slim.min.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script>
		function getHistori(){
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function(){
				if(this.readyState == 4 && this.status == 200){
					document.getElementById("tabelHistory").innerHTML=this.responseText;
				}
			};
			xmlhttp.open("GET","../database/querymanager.php?listHistori="+1,false);
			xmlhttp.send();
		}
		$(document).ready(function(){
			getHistori();
			$(".btn-detail").click(function(){
				var idPerubahan = $(this).attr("data-idperubahan");
				$("#btn_undo").attr("data-idperubahan",idPerubahan);
				console.log(idPerubahan);
				var xmlhttp = new XMLHttpRequest();
				xmlhttp.onreadystatechange = function(){
					if(this.readyState == 4 && this.status == 200){
						$("#tbl_kolom_perubahan").html(this.responseText);
					}
				};
				xmlhttp.open("GET","../database/querymanager.php?listKolomPerubahan="+idPerubahan,false);
				xmlhttp.send();
				$('#exampleModal').modal('show')
			});
			$("#btn_undo").click(function(){
				var idPerubahan = $(this).attr("data-idperubahan");
				var xmlhttp = new XMLHttpRequest();
				xmlhttp.open("GET","../database/editorang.php?idPerubahan="+idPerubahan,false);
				xmlhttp.send();
				$('#exampleModal').modal('hide');
			});
		});
	</script>
</head>
<body>
	<div class="container" style="height:100vh;padding:1px;margin-right: 0;margin-left: 0">
		<div class="row align-items-center justify-content-center" style="height:20vh;margin:0">
			<div class="col">
				<h2>Histori Perubahan</h2>
			</div>
			<div class="col">
				<a class="btn btn-primary text-light" href="dashboard.php">Back</a>
			</div>
		</div>
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Detail Perubahan</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<table class="table table-sm">
							<thead>
								<tr>
									<th scope="col">Kolom</th>
									<th scope="col">Nilai Sebelumnya</th>
								</tr>
							</thead>
							<tbody id="tbl_kolom_perubahan">
								
							</tbody>
						</table>
						<button name="btn_undo" id="btn_undo" class="btn btn-primary text-light">Pulihkan Versi Ini</button>
					</div>
				</div>
			</div>
		</div>
		<div class="row align-items-center justify-content-center" style="height:80vh;margin:0">
			<div class="col">
				<table class="table table-hover">
					<thead>
						<tr>
							<th scope="col">Id Perubahan</th>
							<th scope="col">Id CS</th>
							<th scope="col">Waktu</th>
							<th scope="col">Nama Tabel</th>
							<th scope="col">Id Record</th>
							<th scope="col">Operasi</th>
						</tr>
					</thead>
					<tbody id="tabelHistory">

					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>
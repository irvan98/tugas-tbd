<?php
	include('../database/MySQLDB.php');
	include('session.php');
	if(isset($_GET['idKlien']) && isset($_GET['nama']) &&isset($_GET['idOrang'])){
		$idKlien = $_GET['idKlien'];
		$nama = $_GET['nama'];
		$query = "SELECT nilaiInvestasi,idCS FROM Klien WHERE idKlien ='$idKlien'";
		$data = $database->executeQuery($query);
		$oldvalue = $data[0];
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Ubah Data Klien</title>
	<link rel="stylesheet" href="../css/bootstrap.min.css">
    <script src="../js/jquery-3.3.1.slim.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../jquery/jquery-3.3.1.min.js"></script>
    <script>
    	function getIdCS(){
    		var xmlhttp = new XMLHttpRequest();
    		xmlhttp.onreadystatechange = function(){
    			if(this.readyState == 4 && this.status == 200){
    				document.getElementById("listCS").innerHTML = this.responseText;
    			}
    		};
    		xmlhttp.open("GET","../database/querymanager.php?listCS="+1,false);
    		xmlhttp.send();
    	}
    	function showCurrentClientData(){
    		$("#idCS").val("<?php echo $idCS ?>");
    		$("#idklien").val("<?php echo $_GET['idKlien']?>");
    		$("#idorang").val("<?php echo $_GET['idOrang']?>");
    		$("#name").val("<?php echo $_GET['nama']?>");
    		$("#investasi").val("<?php echo $oldvalue['nilaiInvestasi']?>");
    		var idCS = <?php echo $oldvalue['idCS']?>;
    		console.log(idCS);
    		$("#listCS option[value="+idCS+"]").attr("selected", "selected");
    	}

    	function keepOldData(){
    		$("#old_name").val("<?php echo $_GET['nama']?>");
    		$("#old_investasi").val("<?php echo $oldvalue['nilaiInvestasi']?>");
    		$("#old_cs").val("<?php echo $oldvalue['idCS'] ?>");
    	}
    	$(document).ready(function(){
  			getIdCS();
  			showCurrentClientData();
  			keepOldData();
		});
    </script>
</head>
<body>
	<div class="container" style="height: 100vh">
		<div class="row align-items-center justify-content-center" style="height:100vh">
			<div class="col-5">
				<h2>Ubah Data Klien</h2>
				<form method="POST" action="../database/editorang.php">
					<input type="hidden" name="idCS" id="idCS">
					<div class="form-group">
						<label>Id Klien</label>
						<input class="form-control" type="number" name="idKlien" id="idklien" readonly>
					</div>
					<div class="form-group">
						<label>Id Orang</label>
						<input class="form-control" type="number" name="idOrang" id="idorang" readonly>
					</div>
					<div class="form-group">
						<label>Nama Orang</label>
						<input type="text" class="form-control" name="newvalue[nama]" id="name" readonly>
						<input type="hidden" name="oldvalue[nama]" id="old_name">
					</div>
					<div class="form-group">
						<label>Nilai Investasi</label>
						<input type="number" class="form-control" name="newvalue[nilaiInvestasi]" id="investasi">
						<input type="hidden" name="oldvalue[nilaiInvestasi]"id="old_investasi">
					</div>
					<div class="form-group">
						<label>Id Customer Service</label>
						<select name="newvalue[idCS]" class="form-control" id="listCS"></select>
						<input type="hidden" name="oldvalue[idCS]"id="old_cs">
					</div>

					<input type="submit" class="btn btn-primary" name="btnEditKlien" value="Simpan perubahan"/>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
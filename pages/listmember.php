<!DOCTYPE html>
<html>
<head>
	<title>Daftar Member-MyCRM</title>
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<script src="../js/jquery-3.3.1.slim.min.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script>
		function getMember(){
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function(){
				if(this.readyState == 4 && this.status == 200){
					document.getElementById("tabelKlien").innerHTML=this.responseText;
				}
			};
			xmlhttp.open("GET","../database/querymanager.php?listKlien="+1,false);
			xmlhttp.send();
		}
		$(document).ready(function(){	
			getMember();
			$(".btn-hapus").click(function(){
				console.log("klik hapus");
				var idKlien = $(this).attr("data-idKlien");
				var nama = $(this).attr("data-namaKlien");
				$("#idKlien").val(idKlien);
				$("#namaKlien").val(nama);

				$('#exampleModal').modal('show')
			});
		});

</script>
</head>
<body>
	<div class="container" style="height:100vh">
		<div class="row align-items-center justify-content-center" style="height:20vh;margin:0">
			<div class="col">
				<h2>Daftar Klien</h2>
			</div>
			<div class="col">
				<a class="btn btn-primary text-light" href="addmember.php">Tambahkan Klien Baru</a>
				<a class="btn btn-primary text-light" href="dashboard.php">Back</a>
			</div>
		</div>
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Konfirmasi</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						Apakah Anda yakin akan menonaktifkan klien ini?
						<form method="POST" action="../database/editorang.php">
							<div class="form-group">
								<label>Id Klien</label>
								<input type="text" class="form-control" name="delete_idKlien" id="idKlien" readonly>
							</div>
							<div class="form-group">
								<label>Nama Klien</label>
								<input type="text" class="form-control" id="namaKlien" readonly>
							</div>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
							<button type="submit" name="btnHapusKlien" class="btn btn-danger">Nonaktifkan</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row align-items-center justify-content-center" style="height:80vh">
			<div class="col">
				<?php
						if(isset($_SESSION['delete_success'])){
							echo "<p class='alert alert-info'>".$_SESSION['delete_success']."</p>";
							unset($_SESSION['delete_success']);
						}
					?>
				<table class="table table-hover">
					<thead>
						<tr>
							<th scope="col">Id Klien</th>
							<th scope="col">Id Orang</th>
							<th scope="col">Nama Orang</th>
							<th scope="col">Nilai Investasi</th>
							<th scope="col">Id Customer Service</th>
							<th scope="col">Status</th>
							<th scope="col">Opsi</th>
						</tr>
					</thead>
					<tbody id="tabelKlien">

					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>
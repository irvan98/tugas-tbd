ALTER TABLE RelasiOrang
ADD CONSTRAINT FK_RelasiOrang1_Orang FOREIGN KEY(idOrang1) REFERENCES Orang(idOrang)
ADD CONSTRAINT FK_RelasiOrang2_Orang FOREIGN KEY(idOrang2) REFERENCES Orang(idOrang)

ALTER TABLE Klien
ADD CONSTRAINT FK_Klien_Orang FOREIGN KEY(idOrang) REFERENCES Orang(idOrang)
ADD CONSTRAINT FK_Klien_Cs FOREIGN KEY(idCS) REFERENCES CustomerService(idCS)

ALTER TABLE AnggotaRegion
ADD CONSTRAINT FK_AnggotaRegion1_Region FOREIGN KEY(idParent) REFERENCES Region(idRegion)
ADD CONSTRAINT FK_AnggotaRegion2_Region FOREIGN KEY(idAnak) REFERENCES Region(idRegion)

ALTER TABLE KolomPerubahan
ADD CONSTRAINT FK_KolomPerubahan_History FOREIGN KEY(idPerubahan) REFERENCES History(idPerubahan)

ALTER TABLE History
ADD CONSTRAINT FK_History_Cs FOREIGN KEY(idCS) REFERENCES CustomerService(idCS)
DROP PROCEDURE IF EXISTS rentangPenghasilan;
DELIMITER $$
Create PROCEDURE rentangPenghasilan (IN batasBawah INT,IN batasAtas INT)
BEGIN
	SELECT idOrang,nama,penghasilan FROM Orang 
	WHERE penghasilan>=batasBawah AND penghasilan<=batasAtas;
END $$
DELIMITER ;
DROP PROCEDURE IF EXISTS cariKlien;
DELIMITER $$
CREATE PROCEDURE cariKlien(IN batasBawah INT,IN batasAtas INT,IN opsi INT)
BEGIN
	IF (opsi = 0) THEN
		SELECT Klien.idKlien,Orang.idOrang,Orang.nama,Klien.nilaiInvestasi,Orang.penghasilan,Klien.deleted FROM Klien INNER JOIN Orang ON Orang.idOrang = Klien.idKlien
		WHERE Orang.penghasilan >= batasBawah AND Orang.penghasilan <=batasAtas;
	ELSEIF(opsi = 1) THEN
		SELECT Klien.idKlien,Orang.idOrang,Orang.nama,Klien.nilaiInvestasi,Orang.penghasilan,Klien.deleted FROM Klien INNER JOIN Orang ON Orang.idOrang = Klien.idKlien
		WHERE Klien.nilaiInvestasi >= batasBawah AND Klien.nilaiInvestasi <=batasAtas;
	END IF;
END$$
DELIMITER ;
DROP PROCEDURE IF EXISTS rata2SuatuDaerah;
DELIMITER $$
CREATE PROCEDURE rata2SuatuDaerah(IN nilaiInv BOOLEAN,IN age BOOLEAN,IN namaR VARCHAR(50))
LANGUAGE SQL
BEGIN

DECLARE count INT;

/*buat tabel hasil idRegion yang berada di suatu region*/
DROP TABLE IF EXISTS tblHasil;
CREATE TEMPORARY TABLE IF NOT EXISTS tblHasil(
	idRegion INT
);

INSERT INTO tblHasil
	SELECT
		idRegion
	FROM
		region
    WHERE
		namaRegion = namaR;
	
  
/*buat tabel parent*/
DROP TABLE IF EXISTS tblParent;
CREATE TEMPORARY TABLE IF NOT EXISTS tblParent (
	idRoot INT
);

INSERT INTO tblParent
	SELECT 
		idRegion 
	FROM
		region
    WHERE 
		namaRegion = namaR;

DROP TABLE IF EXISTS tblChildren;
CREATE TEMPORARY TABLE IF NOT EXISTS tblChildren(
	idChildren INT
);

DROP TABLE IF EXISTS tblOrang;
CREATE TEMPORARY TABLE IF NOT EXISTS tblOrang(
	idOrang INT
);

SET count = (SELECT COUNT(*) from tblParent);

WHILE count != 0 DO
INSERT INTO tblChildren
	SELECT idAnak FROM AnggotaRegion
    INNER JOIN tblParent ON AnggotaRegion.idParent = tblParent.idRoot;

INSERT INTO tblHasil
	SELECT DISTINCT idChildren
    FROM tblChildren;
	
	delete from tblParent;

	insert into tblParent
		select
			idChildren
		from
			tblChildren;

DELETE FROM tblChildren;


set count = ((select count(*) from tblParent));
END WHILE;

INSERT INTO tblOrang
	SELECT distinct
		idOrang
	FROM
		Orang INNER JOIN tblHasil ON Orang.lokasi = tblHasil.idRegion;
IF(nilaiInv IS TRUE AND namaR IS NOT NULL) THEN
	SELECT
		AVG(nilaiInvestasi)
	FROM
		Klien
	INNER JOIN
		tblOrang ON tblOrang.idOrang = Klien.idOrang;
ELSEIF(nilaiInv IS TRUE AND namaR is null) THEN
	select
		AVG(nilaiInvestasi)
	from
		Klien;
ELSEIF(age IS TRUE AND namaR is not null) THEN
	select
		AVG(umur)
	from
		tblOrang
	inner join
		Orang on Orang.idOrang = tblOrang.idOrang;
ELSEIF(age IS TRUE AND namaR is null) THEN
	select
		AVG(umur)
	from
		Klien
	inner join
		Orang on Orang.idOrang = Klien.idOrang;
END IF;
END$$
DELIMITER ;
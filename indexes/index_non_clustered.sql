/*Referensi: https://blog.toadworld.com/2017/04/06/speed-up-your-queries-using-the-covering-index-in-mysql*/
/*Indeks Non Clustered untuk tabel orang*/
/*buat sebuah covering index, terurut berdasarkan penghasilan, yang memiliki field penghasilan,lokasi,dan nama*/
ALTER TABLE Orang ADD Index nc_penghasilan_nama(penghasilan,lokasi,nama);

/*Indeks Non Clustered untuk tabel orang*/
/*buat sebuah covering index, terurut berdasarkan penghasilan, yang memiliki field penghasilan,lokasi,dan idOrang*/
ALTER TABLE Orang ADD Index nc_penghasilan_idOrang(penghasilan,lokasi,idOrang);

/*Indeks non Clustered untuk tabel Klien*/
/*buat sebuah covering index, terurut berdasarkan nilaiInvestasi, yang memiliki field nilaiInvestasi,idCs,dan idKlien*/
ALTER TABLE Klien ADD INDEX nc_nilaiInvestasi_idKlien(nilaiInvestasi,idCS,idKlien);
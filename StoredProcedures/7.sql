CREATE PROCEDURE undoPerubahan(IN idP INT)
BEGIN

DECLARE finished INTEGER DEFAULT 0;
DECLARE first BOOLEAN DEFAULT TRUE;
DECLARE idR INT;
DECLARE namaT varchar(50);
DECLARE tempNamaKolom varchar(50);
DECLARE tempTipeData varchar(50);
DECLARE tempNilaiSebelum varchar(50);
DECLARE tmpQuery varchar(100);

DECLARE cursorPerubahan CURSOR FOR SELECT namaKolom,tipeData,nilaiSebelum FROM tblKolomPerubahan;
        
DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;

DROP TABLE IF EXISTS tblKolomPerubahan;

CREATE TEMPORARY TABLE IF NOT EXISTS tblKolomPerubahan(
	namaKolom varchar(50),
    tipeData varchar(50),
    nilaiSebelum varchar(50)
);

SET @str = CONCAT('INSERT INTO tblKolomPerubahan SELECT namaKolom,tipeData,nilaiSebelum FROM kolomperubahan WHERE idPerubahan = ',idP);

PREPARE query1 FROM @str;
EXECUTE query1;
DEALLOCATE PREPARE query1;

OPEN cursorPerubahan;

SET namaT  = (SELECT namaTabel FROM history WHERE idPerubahan=idP);

SET idR = (SELECT idRecord FROM history WHERE idPerubahan = idP);

SET tmpQuery = CONCAT("UPDATE ",namaT," SET ");

get_kolom: LOOP   
 	FETCH cursorPerubahan INTO tempNamaKolom,tempTipeData,tempNilaiSebelum;
 	
 	IF(finished = 1)THEN
 		LEAVE get_kolom;
 	END IF;
 	IF(first IS TRUE)THEN
 		SET first = FALSE;
 	ELSE
 		SET tmpQuery = CONCAT(tmpQuery,',');
 	END IF;
 
 	if(isnumber(tempNilaiSebelum)) THEN
    	SET tmpQuery = CONCAT (tmpQuery,tempNamaKolom,"=",tempNilaiSebelum);
    ELSE
    	SET tmpQuery = CONCAT (tmpQuery,tempNamaKolom,"='",tempNilaiSebelum,"'");
  	END IF;      
END LOOP get_kolom;

CLOSE cursorPerubahan;

if(namaT='Orang') THEN
	SET tmpQuery = CONCAT(tmpQuery," WHERE idOrang=",idR);
ELSEIF(namaT='Klien') THEN
	SET tmpQuery = CONCAT(tmpQuery," WHERE idKlien=",idR);
END IF;

SET @str2 = tmpQuery;
PREPARE query2 FROM @str;
EXECUTE query2;
DEALLOCATE PREPARE query2;

END
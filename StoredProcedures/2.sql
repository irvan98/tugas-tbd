/*SP Untuk menampikan klien pada rentang nilaiInvestasi tertentu
Created By: Frengki Ang */
CREATE PROCEDURE cariKlien(IN batasBawah INT,IN batasAtas INT,IN opsi INT)
BEGIN
	IF (opsi = 0) THEN
		SELECT Klien.idKlien,Orang.idOrang,Orang.nama,Klien.nilaiInvestasi,Orang.penghasilan,Klien.deleted FROM Klien INNER JOIN Orang ON Orang.idOrang = Klien.idKlien
		WHERE Orang.penghasilan >= batasBawah AND Orang.penghasilan <=batasAtas AND deleted = 0;
	ELSEIF(opsi = 1) THEN
		SELECT Klien.idKlien,Orang.idOrang,Orang.nama,Klien.nilaiInvestasi,Orang.penghasilan,Klien.deleted FROM Klien INNER JOIN Orang ON Orang.idOrang = Klien.idKlien
		WHERE Klien.nilaiInvestasi >= batasBawah AND Klien.nilaiInvestasi <=batasAtas AND deleted = 0;
	END IF;
END;
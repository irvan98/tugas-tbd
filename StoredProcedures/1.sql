/*SP untuk menampilkan orang pada rentang penghasilan tertentu
Created By: Frengki Ang*/
Create PROCEDURE rentangPenghasilan (IN batasBawah INT,IN batasAtas INT)
BEGIN
	SELECT idOrang,nama,penghasilan FROM Orang 
	WHERE penghasilan>=batasBawah AND penghasilan<=batasAtas;
END;
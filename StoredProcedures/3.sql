/*SP untuk mencari rata-rata nilai investasi Klien atau umur Orang pada sebuah daerah
atau seluruh daerah, jika parameter namaR bernilai NULL

Created By: Frengki Ang*/
CREATE PROCEDURE rata2SuatuDaerah(IN nilaiInv BOOLEAN,IN age BOOLEAN, IN statusNikah BOOLEAN,IN namaR VARCHAR(50))
LANGUAGE SQL
BEGIN

DECLARE count INT;

/*buat tabel hasil idRegion yang berada di suatu region*/
DROP TABLE IF EXISTS tblHasil;
CREATE TEMPORARY TABLE IF NOT EXISTS tblHasil(
	idRegion INT
);

INSERT INTO tblHasil
	SELECT
		idRegion
	FROM
		region
    WHERE
		namaRegion = namaR;
	
  
/*buat tabel parent*/
DROP TABLE IF EXISTS tblParent;
CREATE TEMPORARY TABLE IF NOT EXISTS tblParent (
	idRoot INT
);

INSERT INTO tblParent
	SELECT 
		idRegion 
	FROM
		region
    WHERE 
		namaRegion = namaR;

DROP TABLE IF EXISTS tblChildren;
CREATE TEMPORARY TABLE IF NOT EXISTS tblChildren(
	idChildren INT
);

DROP TABLE IF EXISTS tblOrang;
CREATE TEMPORARY TABLE IF NOT EXISTS tblOrang(
	idOrang INT
);

SET count = (SELECT COUNT(*) from tblParent);

WHILE count != 0 DO
INSERT INTO tblChildren
	SELECT idAnak FROM AnggotaRegion
    INNER JOIN tblParent ON AnggotaRegion.idParent = tblParent.idRoot;

INSERT INTO tblHasil
	SELECT DISTINCT idChildren
    FROM tblChildren;
	
	delete from tblParent;

	insert into tblParent
		select
			idChildren
		from
			tblChildren;

DELETE FROM tblChildren;


set count = ((select count(*) from tblParent));
END WHILE;

INSERT INTO tblOrang
	SELECT distinct
		idOrang
	FROM
		Orang INNER JOIN tblHasil ON Orang.lokasi = tblHasil.idRegion;

IF(nilaiInv IS TRUE AND namaR IS NOT NULL) THEN
	SELECT
		AVG(nilaiInvestasi) 'rataRata'
	FROM
		Klien
	INNER JOIN
		tblOrang ON tblOrang.idOrang = Klien.idOrang
	where	
		deleted = 0;
ELSEIF(nilaiInv IS TRUE AND namaR is null) THEN
	select
		AVG(nilaiInvestasi) 'rataRata'
	from
		Klien
	where
		deleted = 0;
ELSEIF(age IS TRUE AND namaR is not null) THEN
	select
		AVG(umur) 'rataRata'
	from
		Klien
	inner join
		tblOrang ON tblOrang.idOrang = Klien.idOrang
	inner join
		Orang on Orang.idOrang = tblOrang.idOrang
	where
		deleted = 0;
ELSEIF(age IS TRUE AND namaR is null) THEN
	select
		AVG(umur) 'rataRata'
	from
		Klien
	inner join
		Orang on Orang.idOrang = Klien.idOrang
	where
		deleted = 0;
ELSEIF(statusNikah IS TRUE AND namaR is not null) THEN
	select
		(count(idKlien)*100.0/(select count(idKlien) from Klien) * 1.0)
	from
		Klien
	inner join
		tblOrang ON tblOrang.idOrang = Klien.idOrang
	inner join
		Orang on tblOrang.idOrang = Orang.idOrang
	where	
		statusNikah = 1 AND deleted = 0;
ELSEIF(statusNikah IS TRUE AND namaR is null) THEN
	select	
		(count(idKlien)*100.0/(select count(idKlien) from Klien) * 1.0)
	from
		Klien
	inner join
		Orang on Klien.idOrang = Orang.idOrang
	where
		statusNikah = 1 AND deleted = 0;
END IF;
END





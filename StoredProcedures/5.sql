create procedure cariRelasi(IN idKlien INT)
LANGUAGE SQL
BEGIN

DROP TABLE IF EXISTS tblTemp;
CREATE TEMPORARY TABLE IF NOT EXISTS tblTemp(
	id INT,
	hubungan VARCHAR(50)
);
INSERT INTO tblTemp
SELECT CASE
	WHEN RelasiOrang.idOrang1=@idKlien
		THEN RelasiOrang.idOrang2
	ELSE RelasiOrang.idOrang1
	END AS idOrang,
	RelasiOrang.deskripsi
FROM
	RelasiOrang
INNER JOIN
	Orang ON RelasiOrang.idOrang1 = @idKlien OR RelasiOrang.idOrang2 = @idKlien
WHERE
	Orang.idOrang = @idKlien;

SELECT
	id,
	nama,
	hubungan
FROM
	tblTemp
INNER JOIN
	Orang ON id = Orang.idOrang;
END
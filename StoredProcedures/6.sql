/*Stored Procedure untuk insert ke tabel History*/
CREATE PROCEDURE  tambahHistori(IN idCS INT,IN waktu datetime,IN namaTabel varchar(50),IN idRecord INT,IN kodeOperasi INT)
BEGIN

DECLARE namaOperasi varchar(25);

IF (kodeOperasi = 1) THEN
	SET namaOperasi = "UPDATE";
ELSEIF(kodeOperasi = 2) THEN
	SET namaOperasi = "DELETE";
END IF;

SET @str = CONCAT('INSERT INTO HISTORY (idCS,waktu,namaTabel,idRecord,operasi) VALUES(',idCS,",'",waktu,"','",namaTabel,"',",idRecord,",'",namaOperasi,"')"); 
PREPARE query1 FROM @str;
EXECUTE query1;
deallocate prepare query1;
END
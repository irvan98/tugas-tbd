/*SP untuk mencari orang yang akan berulang tahun N hari kedepan
Created By: Frengki Ang*/
CREATE PROCEDURE eventPenting(IN days INT)
BEGIN
SET @query = CONCAT("SELECT\r\n\tidOrang, DATE_ADD(tanggalLahir, INTERVAL umur YEAR) ,nama \r\nFROM\r\n\tOrang\r\nWHERE\r\n\tDATE_ADD(tanggalLahir, INTERVAL YEAR(CURDATE())-YEAR(tanggalLahir) + IF(DAYOFYEAR(CURDATE()) > DAYOFYEAR(tanggalLahir),1,0)YEAR) BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL ",days," DAY);");

PREPARE temp FROM @query;
EXECUTE temp;
DEALLOCATE PREPARE temp;
END
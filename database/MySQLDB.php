<?php
	class MySQLDB {
		protected $servername = "localhost";
		protected $username = "root";
		protected $password = "";
		protected $dbname = "tugasuastbd";
		
		protected $db_connection;
		
		function openConnection(){
			//create connection
			$this->db_connection = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
			if($this->db_connection->connect_error){
				die('Could not connect to '.$this->servername.' server');
			}else{
				//echo('<p>successfully connected</p>');
			}
			
		}
		
		function executeQuery($sql){
			$this->openConnection();
			$query_result = $this->db_connection->query($sql);
			$result = [];
			if($query_result->num_rows>0){
				//output data of each row
				while($row = $query_result->fetch_array()){
					$result[] = $row;
				}
			}
			$this->db_connection->close();
			return $result;
		}
		
		function executeNonQuery($sql){
			$this->openConnection();
			if($this->db_connection->query($sql) === FALSE){
				die('Query failed: '.$sql);
			}
			$this->db_connection->close();
		}
	}
	$database = new MySQLDB();

?>
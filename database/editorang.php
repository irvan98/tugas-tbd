<?php
	//NB: meskipun nama nya edit orang, tapi file ini juga bertugas melayani edit Klien
	include('session.php');
	include('MySQLDB.php');
	if(isset($_POST['btnEditOrang'])){
		$idCS = $_POST['idCS'];
		$idOrang= $_POST['idOrang'];
		$oldvalue= $_POST['oldvalue'];
		$newvalue= $_POST['newvalue'];
		//echo $idCS."<br>";
		//echo $idOrang."<br>";
		//echo implode(",",$oldvalue)."<br>";
		// foreach ($oldvalue as $key => $value) {
		// 	echo $key." => ".$value."<br>";
		// }
		// echo "<br>";
		// foreach ($newvalue as $key => $value) {
		// 	echo $key." => ".$value."<br>";
		// }
		// echo "<br>";
		//echo implode(",",$newvalue)."<br>";
		//echo sizeof($oldvalue)."<br>";
		//echo sizeof($newvalue)."<br>";
		//echo $database->getLastInsertedId();
		updateOrang($database,$idCS,$idOrang,$oldvalue,$newvalue);
	}

	if(isset($_POST['btnEditKlien'])){
		updateKlien($database,$_POST['idCS'],$_POST['idKlien'],$_POST['idOrang'],$_POST['oldvalue'],$_POST['newvalue']);
	}

	if(isset($_POST['btnHapusKlien'])){
		deleteKlien($database,$idCS,$_POST['delete_idKlien']);
	}

	if(isset($_GET['idPerubahan'])){
		$idPerubahan = $_GET['idPerubahan'];
		revertChanges($database,$idPerubahan);
	}

	function deleteKlien($database,$idCS,$idKlien){
		$query = "UPDATE Klien SET deleted=1 WHERE idKlien=".$idKlien;
		$database->executeNonQuery($query);
		logChangeHistory($database,$idCS,$idKlien,'Klien',[],2);
		$_SESSION['delete_success'] = 'sucessfully deleted klien!';
		header('Location: ../pages/listmember.php');
	}

	function updateKlien($database,$idCS,$idKlien,$idOrang,$oldvalue,$newvalue){
		$diff=array_diff_assoc($oldvalue,$newvalue);
		$first = 1;
		if(sizeof($diff)>0){
			$query = "UPDATE Klien SET ";
			foreach ($diff as $key => $value) {
				//koma untuk separator
				if($first == 1){
					$first = 0;
				}else{
					$query.=",";
				}

				if(!is_numeric($newvalue[$key])){
					$query.=$key."='".$newvalue[$key]."'";
				}else{
					$query.=$key."=".$newvalue[$key];
				}
			}
			$query.=" WHERE idKlien=$idKlien ";
			$database->executeNonQuery($query);
			logChangeHistory($database,$idCS,$idOrang,'Klien',$diff,1);
		}
		header('Location: ../pages/listmember.php');
	}
	
	function updateOrang($database,$idCS,$idOrang,$oldvalue,$newvalue){
		//bandingkan array oldvalue newvalue
		$diff=array_diff_assoc($oldvalue,$newvalue);
		$first = 1;
		if(sizeof($diff)>0){
			$query ="UPDATE Orang SET ";
			foreach ($diff as $key => $value) {
				//koma untuk separator
				if($first == 1){
					$first = 0;
				}else{
					$query.=",";
				}

				if(!is_numeric($newvalue[$key])){
					$query.=$key."='".$newvalue[$key]."'";
				}else{
					$query.=$key."=".$newvalue[$key];
				}
				
			}
			$query.=" WHERE idOrang=$idOrang ";
			$database->executeNonQuery($query);
			logChangeHistory($database,$idCS,$idOrang,'Orang',$diff,1);
		}
		$_SESSION['update_success'] = 'sucess!';
		echo $query;
		header('Location: ../pages/listorang.php');
	}

	///WARNING!: hati hati kalau function ini dipanggil oleh beberapa CS sekaligus
	function logChangeHistory($database,$idCS,$idRecord,$tblName,$diff,$kode){
		//dapatkan datetime sekarang
		date_default_timezone_set("Asia/Jakarta");
		$curr_date=date("Y-m-d H:i:s");
		//echo "Today is " . date("Y-m-d H:i:s") . "<br>";

		//insert ke tabel history
		$query="";
		if($kode==1){
			$query = "CALL tambahHistori('$idCS','$curr_date','$tblName','$idRecord',1)";
		}else if($kode==2){
			$query = "CALL tambahHistori('$idCS','$curr_date','$tblName','$idRecord',2)";
		}
		
		$db = new mysqli("localhost","root","","tugasuastbd");
		$db->query($query);

		//dapatkan id sekarang
		//sepertinya berbahaya kalau diakses lebih dari satu CS bersamaan-> last id nya bisa salah
		//kalau antara insert ke tabel history dan operasi ini ada operasi insert lain
		$query ="SELECT LAST_INSERT_ID()";
		$result = $db->query($query)->fetch_array();
		$last_id =  $result['LAST_INSERT_ID()'];
		$db->close();
		
		//insert ke tabel kolom perubahan
		if(sizeof($diff)>0){
			foreach ($diff as $key => $value) {
				$col_data_type=getColumnDataType($database,$tblName,$key);
				$query = "INSERT INTO kolomperubahan VALUES('$last_id','$key','$col_data_type','$value')";
				$database->executeNonQuery($query);
			}
		}
	}

	function getColumnDataType($database,$tblname,$colname){
		$query = "SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name='$tblname' AND COLUMN_NAME='$colname' ";
		$data=$database->executeQuery($query);
		return $data[0]['DATA_TYPE'];
	}

	function revertChanges($database,$idPerubahan){
		$query = "CALL undoPerubahan('$idPerubahan')";
		$database->executeNonQuery($query);
		$_SESSION['undo_success']='successfuly undo the change!';
	}
?>
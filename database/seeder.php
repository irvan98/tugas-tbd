<?php
	//sebuah file php untuk create tabel dan dummy data
	//file ini menerima request dari initialize.php
 include('MySQLDB.php');

 function createAllTable(){
 	global $database;
 	$query="CREATE TABLE IF NOT EXISTS Orang (
 		idOrang INT AUTO_INCREMENT,
 		nama VARCHAR(50) NOT NULL,
 		jenisKelamin TINYINT(1) NOT NULL,
 		tanggalLahir DATE NOT NULL,
 		umur TINYINT(2) NOT NULL,
 		alamat VARCHAR(50) NOT NULL,
 		lokasi INT NOT NULL,
 		statusNikah TINYINT(1) NOT NULL,
 		penghasilan INT NOT NULL,
 		pekerjaan VARCHAR(50) NOT NULL,
 		alamatEmail VARCHAR(100) NOT NULL,
 		PRIMARY KEY(idOrang)
 	) ENGINE=INNODB";
 	$database->executeNonQuery($query);

 	$query = "CREATE TABLE IF NOT EXISTS Klien(
 		idKlien INT AUTO_INCREMENT,
 		idOrang INT NOT NULL,
 		nilaiInvestasi INT NOT NULL,
 		idCS INT NOT NULL,
 		deleted TINYINT(1) NOT NULL,
 		PRIMARY KEY(idKlien)
 	) ENGINE=INNODB";
 	$database->executeNonQuery($query);

 	$query = "CREATE TABLE IF NOT EXISTS RelasiOrang(
 		idOrang1 INT NOT NULL,
 		idOrang2 INT NOT NULL,
 		deskripsi VARCHAR(50) NOT NULL	
 	) ENGINE=INNODB";
 	$database->executeNonQuery($query);

 	$query = "CREATE TABLE IF NOT EXISTS Region(
 		idRegion INT AUTO_INCREMENT,
 		namaRegion VARCHAR(50) NOT NULL,
 		PRIMARY KEY(idRegion) 
 	) ENGINE=INNODB";
 	$database->executeNonQuery($query);

 	$query = "CREATE TABLE IF NOT EXISTS AnggotaRegion(
 		idParent INT NOT NULL,
 		idAnak INT NOT NULL
 	) ENGINE=INNODB";
 	$database->executeNonQuery($query);

 	$query ="CREATE TABLE IF NOT EXISTS History(
 		idPerubahan INT AUTO_INCREMENT,
 		idCS INT NOT NULL,
 		waktu DATETIME NOT NULL,
 		namaTabel VARCHAR(50) NOT NULL,
 		idRecord INT NOT NULL,
 		operasi VARCHAR(25) NOT NULL,
 		PRIMARY KEY(idPerubahan)
 	) ENGINE=INNODB";
 	$database->executeNonQuery($query);

 	$query="CREATE TABLE IF NOT EXISTS KolomPerubahan(
 		idPerubahan INT NOT NULL,
 		namaKolom VARCHAR(50) NOT NULL,
 		tipeData VARCHAR(50) NOT NULL,
 		nilaiSebelum VARCHAR(50) NOT NULL
 	) ENGINE=INNODB";
 	$database->executeNonQuery($query);

 	$query="CREATE TABLE IF NOT EXISTS CustomerService(
 		idCS INT AUTO_INCREMENT,
 		username VARCHAR(50) NOT NULL,
 		password VARCHAR(50) NOT NULL,
 		namaCS VARCHAR(50) NOT NULL,
 		PRIMARY KEY(idCS)
 	) ENGINE=INNODB";
 	$database->executeNonQuery($query);
 }

 function createDummyData(){
 	global $database;

 	//insert ke tabel region
 	//REFERENCES :https://github.com/edwardsamuel/Wilayah-Administratif-Indonesia/blob/master/mysql/indonesia.sql

 	//insert provinsi terlebih dahulu
 	$query = "INSERT INTO Region(namaRegion)
 		VALUES ('DKI JAKARTA'),
 			   ('JAWA BARAT'),
 			   ('JAWA TENGAH'),
 			   ('DI YOGYAKARTA'),
 			   ('JAWA TIMUR'),
 			   ('BANTEN')";
 	$database->executeNonQuery($query);

 	//insert Kabupaten / Kota
 	$query = "INSERT INTO Region(namaRegion)
 			VALUES ('KABUPATEN KEPULAUAN SERIBU'),
 				   ('KOTA JAKARTA SELATAN'),
 				   ('KOTA JAKARTA TIMUR'),
 				   ('KOTA JAKARTA PUSAT'),
 				   ('KOTA JAKARTA BARAT'),
 				   ('KOTA JAKARTA UTARA'),
 				   ('KABUPATEN BANDUNG'),
 				   ('KABUPATEN BANDUNG BARAT'),
 				   ('KOTA BANDUNG'),
 				   ('KOTA BEKASI'),
 				   ('KABUPATEN GARUT'),
 				   ('KABUPATEN TASIKMALAYA'),
 				   ('KABUPATEN CILACAP'),
 				   ('KABUPATEN BANYUMAS'),
 				   ('KABUPATEN PURBALINGGA'),
 				   ('KABUPATEN BANJARNEGARA'),
 				   ('KABUPATEN KEBUMEN'),
 				   ('KABUPATEN PURWOREJO'),
 				   ('KABUPATEN KULON PROGO'),
 				   ('KABUPATEN BANTUL'),
 				   ('KABUPATEN GUNUNG KIDUL'),
 				   ('KABUPATEN SLEMAN'),
 				   ('KOTA YOGYAKARTA'),
 				   ('KABUPATEN PACITAN'),
 				   ('KABUPATEN PONOROGO'),
 				   ('KABUPATEN TRENGGALEK'),
 				   ('KABUPATEN TULUNGAGUNG'),
 				   ('KABUPATEN TANGERANG'),
 				   ('KOTA CILEGON'),
 				   ('KABUPATEN PANDEGLANG'),
 				   ('KOTA SERANG')";
 	$database->executeNonQuery($query);
 				   
 	//insert ke tabel AnggotaRegion
 	$query = "INSERT INTO AnggotaRegion VALUES (1,7),(1,8),(1,9),(1,10),(1,11),(1,12)";
 	$database->executeNonQuery($query);

 	$query = "INSERT INTO AnggotaRegion VALUES (2,13),(2,14),(2,15),(2,16),(2,17),(2,18)";
 	$database->executeNonQuery($query);

 	$query = "INSERT INTO AnggotaRegion VALUES (3,19),(3,20),(3,21),(3,22),(3,23),(3,24)";
 	$database->executeNonQuery($query);

 	$query = "INSERT INTO AnggotaRegion VALUES (4,25),(4,26),(4,27),(4,28),(4,29)";
 	$database->executeNonQuery($query);

 	$query = "INSERT INTO AnggotaRegion VALUES (5,30),(5,31),(5,32),(5,33)";
 	$database->executeNonQuery($query);

 	$query = "INSERT INTO AnggotaRegion VALUES (6,34),(6,35),(6,36),(6,37)";
 	$database->executeNonQuery($query);

 	//insert ke tabel Orang
 	$query = "INSERT INTO Orang(nama,jenisKelamin,tanggalLahir,umur,alamat,lokasi,statusNikah,penghasilan,pekerjaan,alamatEmail) VALUES('Louis',1,'1999-03-12',20,'Jalan rancabentang I no11b',15,1,1000000,'Mahasiswa','7316003@student.unpar.ac.id')";
 	$database->executeNonQuery($query);

 	$query = "INSERT INTO Orang(nama,jenisKelamin,tanggalLahir,umur,alamat,lokasi,statusNikah,penghasilan,pekerjaan,alamatEmail) VALUES('Frengki Ang',1, '19980831',21,'Jalan rancabentang I no10',15,1,2000000,'Mahasiswa','7316002@student.unpar.ac.id')";
 	$database->executeNonQuery($query);

 	$query = "INSERT INTO Orang(nama,jenisKelamin,tanggalLahir,umur,alamat,lokasi,statusNikah,penghasilan,pekerjaan,alamatEmail) VALUES('Irvan Hardyanto',1,'19980328',21,'Kopo Permai III F22 No.16',15,1,3000000,'Mahasiswa','7316070@student.unpar.ac.id')";
 	$database->executeNonQuery($query);
 	
 	$query = "INSERT INTO Orang(nama,jenisKelamin,tanggalLahir,umur,alamat,lokasi,statusNikah,penghasilan,pekerjaan,alamatEmail) VALUES('Kevin Arnold',1,'19980120',30,'Jalan rancabentang I no221b',15,4,3000000,'Mahasiswa','7316045@student.unpar.ac.id')";
 	$database->executeNonQuery($query);

 	$query = "INSERT INTO Orang(nama,jenisKelamin,tanggalLahir,umur,alamat,lokasi,statusNikah,penghasilan,pekerjaan,alamatEmail) VALUES('Louis',1,'19990312',20,'Jalan rancabentang I no11b',14,3,1000000,'Mahasiswa','7316003@student.unpar.ac.id')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO Orang(nama,jenisKelamin,tanggalLahir,umur,alamat,lokasi,statusNikah,penghasilan,pekerjaan,alamatEmail) VALUES('Susan Tycoson',0,'19720204',36,'Jalan Merdeka II No.30',14,5,30000000,'Pemilik Restoran','SsssusanTyk282@gmail.com')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO Orang(nama,jenisKelamin,tanggalLahir,umur,alamat,lokasi,statusNikah,penghasilan,pekerjaan,alamatEmail) VALUES('Mikel Tinora',1,'19970131',22,'Jalan Rancabulan No.10',13,5,2500000,'Mahasiswa','Mikaelsuper9000@gmail.com')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO Orang(nama,jenisKelamin,tanggalLahir,umur,alamat,lokasi,statusNikah,penghasilan,pekerjaan,alamatEmail) VALUES('Michelle',0,'19950721',24,'Jalan Diponegoro',16,8,3700000,'Pengangguran','Michellemichellin@gmail.com')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO Orang(nama,jenisKelamin,tanggalLahir,umur,alamat,lokasi,statusNikah,penghasilan,pekerjaan,alamatEmail) VALUES('Fazua Milano Ossas',1,'19891205',30,'Jalan Kesehatan I DOK II',16,10,4100000,'Nelayan','Fazuamilano123@gmail.com')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO Orang(nama,jenisKelamin,tanggalLahir,umur,alamat,lokasi,statusNikah,penghasilan,pekerjaan,alamatEmail) VALUES('Karmelia',0,'19980313',21,'Jalan Ramba',17,7,2200000,'Mahasiswa','Karmekarmelia11@gmail.com')";
	$database->executeNonQuery($query);

	$query ="INSERT INTO Orang(nama,jenisKelamin,tanggalLahir,umur,alamat,lokasi,statusNikah,penghasilan,pekerjaan,alamatEmail) VALUES ('Morgan Ross',1,'1970201',49,'Jalan Surga II No.310',18,10,8500000,'Pemilik Toko Serba Guna','morgan2005@gmail.com')";
	$database->executeNonQuery($query);
	
	$query ="INSERT INTO Orang(nama,jenisKelamin,tanggalLahir,umur,alamat,lokasi,statusNikah,penghasilan,pekerjaan,alamatEmail) VALUES ('Bob Ross',1,'19680421',51,'Jalan Surga II No.310',19,10,6000000,'Pelukis','Bob1988@gmail.com')";
	$database->executeNonQuery($query);

	$query ="INSERT INTO Orang(nama,jenisKelamin,tanggalLahir,umur,alamat,lokasi,statusNikah,penghasilan,pekerjaan,alamatEmail) VALUES ('Steven Ross',1,'19661102',53,'Jalan Surga II No.310',19,10,4200000,'Guru','StevenRoss66@gmail.com')";
	$database->executeNonQuery($query);

	$query ="INSERT INTO Orang(nama,jenisKelamin,tanggalLahir,umur,alamat,lokasi,statusNikah,penghasilan,pekerjaan,alamatEmail) VALUES ('Lynda Brown',0,'19431121',76,'Jalan Surga II No.310',20,10,0,'Pengangguran','LyndaRoss@gmail.com')";
	$database->executeNonQuery($query);

	$query ="INSERT INTO Orang(nama,jenisKelamin,tanggalLahir,umur,alamat,lokasi,statusNikah,penghasilan,pekerjaan,alamatEmail) VALUES ('Martin Cob',1,'19700518',49,'Jalan Merdeka II No.30',20,5,12000000,'Sales Manager','Martin12345@gmail.com')";
	$database->executeNonQuery($query);

 	//insert ke tabel RelasiOrang
 	$query ="INSERT INTO RelasiOrang VALUES(1,2,'Teman')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO RelasiOrang VALUES(1,3,'Teman')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO RelasiOrang VALUES(2,3,'Teman')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO RelasiOrang VALUES(5,6,'Nikah')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO RelasiOrang VALUES(4,5,'Teman')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO RelasiOrang VALUES(3,8,'Teman')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO RelasiOrang VALUES(1,10,'Teman')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO RelasiOrang VALUES(2,9,'Teman')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO RelasiOrang VALUES(2,7,'Teman')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO RelasiOrang VALUES(7,9,'Teman')";
	$database->executeNonQuery($query);
	 
	$query ="INSERT INTO RelasiOrang VALUES (5,11,'Ayah')";
	$database->executeNonQuery($query);

	$query ="INSERT INTO RelasiOrang VALUES (5,12,'Ayah')";
	$database->executeNonQuery($query);

	$query ="INSERT INTO RelasiOrang VALUES (5,13,'Ayah')";
	$database->executeNonQuery($query);
	
	$query ="INSERT INTO RelasiOrang VALUES (5,14,'Nikah')";
	$database->executeNonQuery($query);

	$query ="INSERT INTO RelasiOrang VALUES (4,15,'Nikah')";
	$database->executeNonQuery($query);

 	//insert ke tabel Klien
 	$query ="INSERT INTO Klien(idOrang,nilaiInvestasi,idCS,deleted) VALUES(1,550000,1,0)";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO Klien(idOrang,nilaiInvestasi,idCS,deleted) VALUES(2,1550000,1,0)";
 	$database->executeNonQuery($query);	

 	$query ="INSERT INTO Klien(idOrang,nilaiInvestasi,idCS,deleted) VALUES(3,350000,1,0)";
 	$database->executeNonQuery($query);	

 	$query ="INSERT INTO Klien(idOrang,nilaiInvestasi,idCS,deleted) VALUES(4,650000,1,0)";
 	$database->executeNonQuery($query);	

 	$query ="INSERT INTO Klien(idOrang,nilaiInvestasi,idCS,deleted) VALUES(5,1000000,1,0)";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO Klien(idOrang,nilaiInvestasi,idCS,deleted) VALUES(6,950000,1,0)";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO Klien(idOrang,nilaiInvestasi,idCS,deleted) VALUES(7,1250000,1,0)";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO Klien(idOrang,nilaiInvestasi,idCS,deleted) VALUES(8,40000,1,0)";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO Klien(idOrang,nilaiInvestasi,idCS,deleted) VALUES(9,150000,1,0)";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO Klien(idOrang,nilaiInvestasi,idCS,deleted) VALUES(10,250000,1,0)";
 	$database->executeNonQuery($query);

 	//insert ke tabel customer service
 	$query ="INSERT INTO CustomerService(username,password,namaCS) VALUES('louis','123456789','Louis Genio')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO CustomerService(username,password,namaCS) VALUES('frengki','unpar94','Frengki Ang')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO CustomerService(username,password,namaCS) VALUES('irvan98','d3d3g3m3z','Irvan Hardyanto')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO CustomerService(username,password,namaCS) VALUES('vckevin98','moes','Kevin Arnold')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO CustomerService(username,password,namaCS) VALUES('hashrul123','verochan','Hashrul')";
 	$database->executeNonQuery($query);

 	$query ="INSERT INTO CustomerService(username,password,namaCS) VALUES('gio234','elisatiganteng','Giovani Anggasta')";
 	$database->executeNonQuery($query);
 }

 function addConstraints(){
 	global $database;
	 $query ="ALTER TABLE Orang
			ADD CONSTRAINT FK_Orang_Region
			FOREIGN KEY (lokasi) REFERENCES Region(idRegion)";
	$database->executeNonQuery($query);
 }

 function addStoredProcedure(){
 	global $database;
 	$query =file_get_contents("../StoredProcedures/1.sql");
 	$database->executeNonQuery($query);
 	$query =file_get_contents("../StoredProcedures/2.sql");
 	$database->executeNonQuery($query);
 	$query =file_get_contents("../StoredProcedures/3.sql");
 	$database->executeNonQuery($query);
 	$query =file_get_contents("../StoredProcedures/4.sql");
 	$database->executeNonQuery($query);
 	$query =file_get_contents("../StoredProcedures/5.sql");
 	$database->executeNonQuery($query);
 	$query =file_get_contents("../StoredProcedures/6.sql");
 	$database->executeNonQuery($query);
 	$query =file_get_contents("../function.sql");
 	$database->executeNonQuery($query);
 	$query =file_get_contents("../StoredProcedures/7.sql");
 	$database->executeNonQuery($query);
 }

 if(isset($_POST['btnInsertData'])){
 	createDummyData();
 	addStoredProcedure();
 	header("Location: ../pages/initialize.php");
 }
 if(isset($_POST['btnTabel'])){
 	createAllTable();
 	header("Location: ../pages/initialize.php");
 }
?>
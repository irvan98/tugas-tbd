<?php 
	//file php untuk memproses query-query dan request dari halaman
	include('MySQLDB.php');

	//data orang dikirim dalam sebuah associative array..
	if(isset($_POST['btnAddOrang'])){
		//echo('you clicked me!'.'</br>');
		$arr_orang = $_POST['attributOrang'];
		insertOrang($database,$arr_orang);
	}

	if(isset($_POST['btnAddKlien'])){
		$arr_klien = $_POST['attributKlien'];
		insertKlien($database,$arr_klien);
	}

	if(isset($_GET['idOrang'])){
		$idOrang = $_GET['idOrang'];
		getNama($database,$idOrang);
	}

	if(isset($_GET['listId'])){
		getIdOrang($database);
	}

	if(isset($_GET['listOrang'])){
		getOrang($database);
	}

	if(isset($_GET['listHistori'])){
		getHistori($database);
	}

	if(isset($_GET['listRegion'])){
		getRegion($database);
	}

	if(isset($_GET['listKlien'])){
		getMember($database);
	}

	if(isset($_GET['listCS'])){
		getIdCS($database);
	}

	if(isset($_GET['listKlienRentangPenghasilan'])){
		$query_param =explode(",",$_GET['listKlienRentangPenghasilan']);
		getKlien($database,$query_param[0],$query_param[1],0);
	}

	if(isset($_GET['listKlienRentangInvestasi'])){
		$query_param =explode(",",$_GET['listKlienRentangInvestasi']);
		getKlien($database,$query_param[0],$query_param[1],1);
	}

	if(isset($_GET['listOrangRentangPenghasilan'])){
		$query_param =explode(",",$_GET['listOrangRentangPenghasilan']);
		getOrangRentangPenghasilan($database,$query_param[0],$query_param[1]);
	}

	if(isset($_GET['listEvent'])){
		$query_param =explode(",",$_GET['listEvent']);
		getUlangTahunOrang($database,$query_param[0]);
	}

	if(isset($_GET['listRelasi'])){
		$query_param =explode(",",$_GET['listRelasi']);
		getListRelasi($database,$query_param[0]);
	}

	if(isset($_GET['listKolomPerubahan'])){
		$idPerubahan = $_GET['listKolomPerubahan'];
		getKolomPerubahan($database,$idPerubahan);
	}

	if(isset($_GET['rata2Inv'])){
		$query_param =explode(",",$_GET['rata2Inv']);
		getRata2Inv($database,$query_param[0]);
	}

	if(isset($_GET['allRata2Inv'])){
		getAllRata2Inv($database);
	}

	if(isset($_GET['rata2Umur'])){
		$query_param =explode(",",$_GET['rata2Umur']);
		getRata2Umur($database,$query_param[0]);
	}

	if(isset($_GET['allRata2Umur'])){
		getAllRata2Umur($database);
	}

	if(isset($_GET['persenNikah'])){
		$query_param =explode(",",$_GET['persenNikah']);
		getPersenNikah($database,$query_param[0]);
	}

	if(isset($_GET['allPersenNikah'])){
		getAllPersenNikah($database);
	}

	if(isset($_GET['karakteristik1'])){
		$query_param =explode(",",$_GET['karakteristik1']);
		getKarakteristik1($database,$query_param[0]);
	}

	if(isset($_GET['karakteristik2'])){
		$query_param =explode(",",$_GET['karakteristik2']);
		getKarakteristik2($database,$query_param[0]);
	}

	function insertOrang($database,$arr){
		$first=1;
		$query = "INSERT INTO Orang(nama,jenisKelamin,tanggalLahir,umur,alamat,lokasi,statusNikah,penghasilan,pekerjaan,alamatEmail) VALUES(";
		foreach ($arr as $key => $value) {
			echo ('key:'.$key.' value: '.$value.'</br>');

			//koma untuk separator
			if($first == 1){
				$first = 0;
			}else{
				$query.=",";
			}
			if(!is_numeric($arr[$key])){
				$query.="'".$arr[$key]."'";
			}else{
				$query.=$arr[$key];
			}		
		}
		$query = $query.')';
		echo ($query);
		$database->executeNonQuery($query);
		header("Location: ../pages/listorang.php");
	}

	function getNama($database,$idOrang){
		$query = "SELECT nama FROM Orang WHERE idOrang =".$idOrang;
		$data = $database->executeQuery($query);
		$row = $data[0];
		echo $row['nama'];
	}

	function getIdOrang($database){
		$query ="SELECT idOrang FROM Orang";
		$data = $database->executeQuery($query);
		for($i=0;$i<sizeof($data);$i++){
			$row = $data[$i];
			echo "<option value=".$row['idOrang'].">".$row['idOrang']."</option>";
		}
	}

	function getOrang($database){
		$query = "SELECT * FROM Orang INNER JOIN Region WHERE Orang.lokasi = Region.idRegion";
		$data = $database->executeQuery($query);
		for($i=0;$i<sizeof($data);$i++){
			$row = $data[$i];
			echo "<tr>";
			echo "<td>".$row['idOrang']."</td>";
			echo "<td>".$row['nama']."</td>";
			if($row['jenisKelamin']==0){
				echo "<td>Wanita</td>";
			}else{
				echo "<td>Pria</td>";
			}
			echo"<td>".$row['tanggalLahir']."</td>";
			echo"<td>".$row['umur']."</td>";
			echo"<td>".$row['alamat']."</td>";
			echo"<td>".$row['namaRegion']."</td>";
			if($row['statusNikah']==1){
				echo "<td>Sudah Nikah</td>";
			}else{
				echo "<td>Belum Nikah</td>";
			}
			echo"<td>".$row['penghasilan']."</td>";
			echo"<td>".$row['pekerjaan']."</td>";
			echo"<td>".$row['alamatEmail']."</td>";
			echo"<td><a class='btn btn-primary text-light' href='editorang.php?idOrang=".$row['idOrang']."'>Edit</a></td>";
			echo "</tr>";
		}
	}

	function getHistori($database){
		$query = "SELECT * FROM History";
		$data = $database->executeQuery($query);
		for($i=0;$i<sizeof($data);$i++){
			$row = $data[$i];
			echo "<tr>";
			echo "<td>".$row[0]."</td>";
			echo "<td>".$row[1]."</td>";
			echo "<td>".$row[2]."</td>";
			echo "<td>".$row[3]."</td>";
			echo "<td>".$row[4]."</td>";
			echo "<td>".$row[5]."</td>";
			echo "<td><button type='button' class='btn btn-primary btn-detail' data-idPerubahan=".$row[0].">
  Lihat Detail
</button></td>";
			echo "</tr>";
		}
	}

	function getKolomPerubahan($database,$idPerubahan){
		$query ="SELECT idPerubahan,namaKolom,tipeData,nilaiSebelum FROM kolomperubahan WHERE idPerubahan =".$idPerubahan;
		$data = $database->executeQuery($query);
		for($i=0;$i<sizeof($data);$i++){
			$row = $data[$i];
			echo "<tr>";
			echo"<td>".$row['namaKolom']."</td>";
			echo"<td>".$row['nilaiSebelum']."</td>";
			echo "</tr>";
		}
	}

	function getUlangTahunOrang($database,$days){
		$query = "CALL eventPenting('$days')";
		$data = $database->executeQuery($query);

		for($i=0;$i<sizeof($data);$i++){
			$row = $data[$i];
			echo "<tr>";
			echo "<td>".$row[0]."</td>";
			echo"<td>".$row[1]."</td>";
			echo "<td> Ulang Tahun ".$row[2]."</td>";
			echo "</tr>";
		}
	}

	function getRegion($database){
		$query = "SELECT idRegion,namaRegion FROM Region";
		$ctr = 1;
		$data = $database->executeQuery($query);
		for($i=0;$i<sizeof($data);$i++){
			$row = $data[$i];
			echo "<option value=".$row['idRegion']." id=".$ctr.">".$row['idRegion']." - ".$row['namaRegion']."</option>";
			$ctr++;
		}
	}

	function getMember($database){
		$query = "SELECT Klien.idKlien,Klien.idOrang,Orang.nama,Klien.nilaiInvestasi,Klien.idCS,Klien.deleted FROM Klien INNER JOIN Orang ON Orang.idOrang = Klien.idOrang";
		$data = $database->executeQuery($query);
		for($i=0;$i<sizeof($data);$i++){
			$row = $data[$i];
			echo "<tr>";
			echo "<td>".$row['idKlien']."</td>";
			echo "<td>".$row['idOrang']."</td>";
			echo "<td>".$row['nama']."</td>";
			echo "<td>".$row['nilaiInvestasi']."</td>";
			echo "<td>".$row['idCS']."</td>";
			if($row['deleted']==0){
				echo "<td>Aktif</td>";
				echo"<td><a class='btn btn-primary text-light' href='editklien.php?idKlien=".$row['idKlien']."&nama=".$row['nama']."&idOrang=".$row['idOrang']."'>Edit</a>&nbsp &nbsp<button type='button' class='btn btn-danger btn-hapus'  data-idKlien=".$row['idKlien']." data-namaKlien='".$row['nama']."'>
  Nonaktifkan
</button></td>";
			}else{
				echo "<td><strong>TIDAK AKTIF</strong></td>";
				echo"<td><button class='btn btn-primary text-light' href='editklien.php?idKlien=".$row['idKlien']."&nama=".$row['nama']."&idOrang=".$row['idOrang']."' disabled>Edit</button>&nbsp &nbsp<button type='button' class='btn btn-danger btn-hapus'  data-idKlien=".$row['idKlien']." data-namaKlien='".$row['nama']."' disabled>
  Nonaktifkan
</button></td>";
			}
			echo "</tr>";
		}
	}

	//$mode= 0 -> cari klien dgn rentang penghasilan tertentu
	//$mode= 1 -> cari klien dgn rentang investasi tertentu	
	function getKlien($database,$batasBawah,$batasAtas,$mode){
		$query ="CALL cariKlien('$batasBawah','$batasAtas','$mode')";
		$data = $database->executeQuery($query);
		for($i=0;$i<sizeof($data);$i++){
			$row = $data[$i];
			echo "<tr>";
			echo "<td>".$row['idKlien']."</td>";
			echo "<td>".$row['nama']."</td>";
			echo "<td>".$row['nilaiInvestasi']."</td>";
			echo "<td>".$row['penghasilan']."</td>";
			if($row['deleted']==0){
				echo "<td>Aktif</td>";
			}else{
				echo "<td>Tidak Aktif</td>";
			}
			echo "</tr>";
		}
	}

	function getOrangRentangPenghasilan($database,$batasBawah,$batasAtas){
		$query ="CALL rentangPenghasilan('$batasBawah','$batasAtas')";
		$data = $database->executeQuery($query);
		for($i=0;$i<sizeof($data);$i++){
			$row = $data[$i];
			echo "<tr>";
			echo "<td>".$row['idOrang']."</td>";
			echo "<td>".$row['nama']."</td>";
			echo "<td>".$row['penghasilan']."</td>";
			echo "</tr>";
		}
	}

	function getListRelasi($database,$idKlien){
		$query ="CALL cariRelasi('$idKlien')";
		$data = $database->executeQuery($query);
		for($i=0;$i<sizeof($data);$i++){
			$row = $data[$i];
			echo "<tr>";
			echo "<td>".$row['id']."</td>";
			echo "<td>".$row['nama']."</td>";
			echo "<td>".$row['hubungan']."</td>";
			echo "</tr>";
		}
	}

	function getIdCS($database){
		$query = "SELECT idCS,namaCS FROM customerservice";
		$data = $database->executeQuery($query);
		for($i=0;$i<sizeof($data);$i++){
			$row = $data[$i];
			echo "<option value=".$row['idCS'].">".$row['idCS']." - ".$row['namaCS']."</option>";
		}
	}
	
	function insertKlien($database,$arr){
		$first = 1;
		$query = "INSERT INTO Klien(idOrang,nilaiInvestasi,idCS,deleted) VALUES(";
		foreach ($arr as $key => $value) {
			//koma untuk separator
			if($first == 1){
				$first = 0;
			}else{
				$query.=",";
			}
			if(!is_numeric($arr[$key])){
				$query.="'".$arr[$key]."'";
			}else{
				$query.=$arr[$key];
			}		
		}
		$query = $query.')';
		$database->executeNonQuery($query);
		header("Location: ../pages/listmember.php");
	}

	function getRata2Inv($database,$nama_lokasi){
		$query = "CALL rata2SuatuDaerah(1,0,0,'$nama_lokasi')";
		$data = $database->executeQuery($query);
		$row = $data[0];
		echo "<tr>";
		echo "<td>".$row[0]."</td>";
		echo "</tr>";
	}

	function getAllRata2Inv($database){
		$var = null;
		$query = "CALL rata2SuatuDaerah(1,0,0,$var)";
		$data = $database->executeQuery($query);
		$row = $data[0];
		echo "<tr>";
		echo "<td>".$row[0]."</td>";
		echo "</tr>";
	}

	function getRata2Umur($database,$nama_lokasi){
		$query = "CALL rata2SuatuDaerah(0,1,0,'$nama_lokasi')";
		$data = $database->executeQuery($query);
		$row = $data[0];
		echo "<tr>";
		echo "<td>".$row[0]."</td>";
		echo "</tr>";
	}

	function getAllRata2Umur($database){
		$var = null;
		$query = "CALL rata2SuatuDaerah(0,1,0,$var)";
		$data = $database->executeQuery($query);
		$row = $data[0];
		echo "<tr>";
		echo "<td>".$row[0]."</td>";
		echo "</tr>";
	}

	function getPersenNikah($database,$nama_lokasi){
		$query = "CALL rata2SuatuDaerah(0,0,1,'$nama_lokasi')";
		$data = $database->executeQuery($query);
		$row = $data[0];
		echo "<tr>";
		echo "<td>".$row[0]."</td>";
		echo "</tr>";
	}

	function getAllPersenNikah($database){
		$var = null;
		$query = "CALL rata2SuatuDaerah(0,0,1,$var)";
		$data = $database->executeQuery($query);
		$row = $data[0];
		echo "<tr>";
		echo "<td>".$row[0]."</td>";
		echo "</tr>";
	}

	function getKarakteristik1($database,$nama_lokasi){
		$query = "CALL rata2SuatuDaerah(1,0,0,'$nama_lokasi')";
		$data = $database->executeQuery($query);
		$row = $data[0];
		echo "<tr>";
		echo "<td>".$row[0]."</td>";
		$query = "CALL rata2SuatuDaerah(0,1,0,'$nama_lokasi')";
		$data = $database->executeQuery($query);
		$row = $data[0];
		echo "<td>".$row[0]."</td>";
		$query = "CALL rata2SuatuDaerah(0,0,1,'$nama_lokasi')";
		$data = $database->executeQuery($query);
		$row = $data[0];
		echo "<td>".$row[0]."</td>";
		echo "</tr>";
	}

	function getKarakteristik2($database,$nama_lokasi){
		$query = "CALL rata2SuatuDaerah(1,0,0,'$nama_lokasi')";
		$data = $database->executeQuery($query);
		$row = $data[0];
		echo "<tr>";
		echo "<td>".$row[0]."</td>";
		$query = "CALL rata2SuatuDaerah(0,1,0,'$nama_lokasi')";
		$data = $database->executeQuery($query);
		$row = $data[0];
		echo "<td>".$row[0]."</td>";
		$query = "CALL rata2SuatuDaerah(0,0,1,'$nama_lokasi')";
		$data = $database->executeQuery($query);
		$row = $data[0];
		echo "<td>".$row[0]."</td>";
		echo "</tr>";
	}
?>